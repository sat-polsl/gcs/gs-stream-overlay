import requests, sys, PyQt5.QtCore
from PyQt5.QtWidgets import * 
from PyQt5.QtGui import * 
import Observation, MainWindow

# Here you can paste your satnogs station number
STATION_NUMBER = '1712'
# Refreshing every 60 seconds
REFRESH_EVERY = 60000

# With this function we downoad all the observations from SatNOGS API and return them as a list of Observation class objects
def RefreshObservations():
    url = 'https://network.satnogs.org/api/'
    params = dict(format='json', ground_station=STATION_NUMBER)
    resp = requests.get(url + 'jobs/', params)
    data = resp.json()
    observation_list = []
    for x in range(len(data)):
        observation_list.append(Observation.Observation(data[x]['id'], data[x]['tle0'], data[x]['start'], data[x]['end'], data[x]['frequency']))
    return observation_list


# This function is called to collect the data from the API and display all the observations
# It also turns on the timer that calls the function every REFRESH_EVERY ms
def show():
    observations_list = RefreshObservations()
    observations_list.sort(key = lambda x: x.getRemainingSeconds())
    window.displayObservations(observations_list)
    window.show()
    timer = PyQt5.QtCore.QTimer()
    timer.timeout.connect(show)
    timer.start(REFRESH_EVERY)
    return timer

app = QApplication(sys.argv)
window = MainWindow.MainWindow()
timer = show()
app.exec()
   

