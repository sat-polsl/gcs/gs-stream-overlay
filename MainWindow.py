from PyQt5.QtWidgets import * 
from PyQt5.QtGui import * 

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        #self.setWindowFlags(PyQt5.QtCore.Qt.FramelessWindowHint)
        self.setFixedSize(1000,1000)
        self.setStyleSheet("background-color: black;")
    
    def displayObservations(self, observations):
        self.container = QWidget(self)
        self.setCentralWidget(self.container)
        self.scheduled_observations = QLabel('Scheduled observations:', self.container)
        self.scheduled_observations.move(20, 20)
        self.scheduled_observations.setFont(QFont('Arial', 20))
        self.scheduled_observations.setStyleSheet("color: white")

        number_of_observations = len(observations) if len(observations) < 5 else  5

        for i in range(number_of_observations):
            self.createRow(observations[i], i, self.container)
        

# Displaying the most important information about every observation
    def createRow(self, observation, i, parent):    

        observation_label = QLabel('Name: ' + observation.getName(), parent)
        observation_label.move(30, 80  + i * 190)
        observation_label.setFont(QFont('Arial', 20))
        observation_label.setContentsMargins(5,5,5,5)
        observation_label.setStyleSheet("background-color: #03366f; color: white; padding: 5")
        observation_label.adjustSize()

        if observation.getRemainingSeconds() >= 3600:
            remainging_time_string = 'in ' + str(int(observation.getRemainingSeconds() // 3600))+ ' h ' + str(int((observation.getRemainingSeconds() % 3600) // 60)) + ' min'
        elif observation.getRemainingSeconds() < 3600 and observation.getRemainingSeconds() // 60 != 0:
            remainging_time_string = 'in ' + str(int(observation.getRemainingSeconds() // 60)) + ' min'
        else:
            remainging_time_string = 'starting...'


        starting_in_label = QLabel(remainging_time_string, parent)
        starting_in_label.move(600, 80  + i * 190)
        starting_in_label.setFont(QFont('Arial', 20))
        starting_in_label.setContentsMargins(5,5,5,5)
        starting_in_label.setStyleSheet("background-color: #1c91bd; color: white; padding: 5")
        starting_in_label.adjustSize()      
        
        freq_label = QLabel('Frequency: ' + str(observation.getFrequency()) + 'MHz', parent)
        freq_label.move(30, 150 + i * 190)
        freq_label.setFont(QFont('Arial', 20))
        freq_label.setContentsMargins(5,5,5,5)
        freq_label.setStyleSheet("background-color: #6ab053; color: white; padding: 5")
        freq_label.adjustSize()

        id_label = QLabel('ID: ' + str(observation.getId()), parent)
        id_label.move(600, 150 + i * 190)
        id_label.setFont(QFont('Arial', 20))
        id_label.setContentsMargins(5,5,5,5)
        id_label.setStyleSheet("background-color: #eab676; color: white; padding: 5")
        id_label.adjustSize()

        return [observation_label, starting_in_label, freq_label, id_label]