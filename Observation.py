from datetime import datetime
class Observation():
    def __init__(self, id, name, start, end, freq) -> None:
        self.id = id
        self.name = name
        self.start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
        self.end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
        self.freq = freq/1000000
        self.remaining_time = self.calculateRemainingTime()

    def calculateRemainingTime(self):
        return (self.start - datetime.utcnow()).total_seconds()

    def getAllData(self):
        return 'ID: ' + str(self.id) + ' name: ' + self.name + ' start: ' + str(self.start) + ' end: ' + str(self.end) + ' freq: ' + str(self.freq) + ' remaining time: ' + str(self.remaining_time)

    def getName(self):
        return self.name

    def getId(self):
        return self.id

    def getRemainingSeconds(self):
        return self.remaining_time

    def getFrequency(self):
        return self.freq